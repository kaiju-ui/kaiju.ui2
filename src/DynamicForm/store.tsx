import {Request} from "../Utils"
import {Form} from "../Form";
import i18n from "i18next";
import {Message, useToaster} from "rsuite";
import React, {useState} from "react";
import {useFormik} from "formik";

interface RequestInterface {
    method: string,
    params: { [key: string]: any }
}

interface DFIterface {
    disableGroup?: boolean;
    loadRequest: RequestInterface;
    loadExtraParams?: any;
    saveRequest: RequestInterface;
    successCallback: (props: any) => void;
    onLoad?: (props: any) => void;
    autoLoad?: boolean;
    toaster?: any;
    saveDiff?: boolean;
    isNested?: boolean;

}

class DynamicFormStore {
    disableGroup?: boolean;
    loadRequest: RequestInterface;
    loadExtraParams?: any;
    saveRequest: RequestInterface;
    successCallback: any;
    formStore: any;
    onLoad?: (props: any) => void;
    toaster?: any;
    autoLoad: boolean = false;
    isNested: boolean = false;
    saveDiff?: boolean = false;

    constructor({saveDiff=true, ...props}: DFIterface) {
        /* {
            disableGroup: true,
            loadRequest: {
                method: 'Dome method',
                params: {
                    param1: "PARAM 1"
                }
            },
            loadExtraParams: {},
            saveRequest: {
                method: 'Dome method',
                params: {
                    formParam: "PARAM 1"
                }
            },
            successCallback: (response) => {
                console.log(response)
            }
         }
        */

        this.disableGroup = props.disableGroup || false;
        this.toaster = props.toaster;
        this.isNested = props.isNested || this.isNested;
        this.saveDiff = saveDiff;
        this.loadRequest = props.loadRequest;
        this.loadExtraParams = props.loadExtraParams;
        this.saveRequest = props.saveRequest;
        this.successCallback = props.successCallback;
        this.onLoad = props.onLoad;
        this.autoLoad = props.autoLoad || this.autoLoad;
        this.formStore = new Form.FormStore({
            fields: [],
            loadExtraParams: this.loadExtraParams,
            disableGroups: true,
            onSubmit: (values: any, formikState: any, changedValues: any) => {
                this.save(values, formikState, changedValues)
            }
        });
        if (this.autoLoad) {
            this.load()
        }
    }

    load() {
        Request(this.loadRequest.method, this.loadRequest.params).then((response: any) => {
            if (response.result) {
                const fields = response.result.map((group: any) => {
                    return {
                        id: group.id || group.key,
                        ...group
                    }
                });
                const valStructure = this.formStore.initFields(fields);
                if (this.onLoad) {
                    this.onLoad(valStructure)
                }
            }
        })
    }

    save(values: any, formikState: any, changedValues: any) {
        let params = {}
        if (this.saveDiff) {
            params = {
                ...this.saveRequest.params,
                ...changedValues
            };
        } else {
            params = {
                ...this.saveRequest.params,
                ...values
            };
        }
        if (this.isNested){
            params = {data: params}
        }

        Request(this.saveRequest.method, params).then((response: any) => {
            if (response.error?.code == 422) {
                const errors: { [key: string]: string[] } = {};

                Object.keys(response.error?.data?.fields || {}).forEach((key: any) => {
                    const fieldsErrors: any[] = response.error?.data?.fields[key];
                    const fullErrors: string[] = [];
                    fieldsErrors.forEach((error => {
                        fullErrors.push(i18n.t(error.code).toString())
                    }));
                    errors[key] = fullErrors
                });
                formikState.setErrors(errors);

                if (this.toaster) {
                    this.toaster.push(
                        (<Message duration={4000} showIcon type={'error'}>
                            {i18n.t('error.validation-error-title')}
                        </Message>),
                        {placement: "topEnd"}
                    )
                }
            } else if (response.error) {
                if (this.toaster) {
                    this.toaster.push(
                        (<Message duration={4000} showIcon type={'error'}>
                            <h5>{i18n.t('error.internal-error')}</h5>
                        </Message>),
                        {placement: "topEnd"}
                    )
                }
            } else if (response.result) {
                this.successCallback(response, this)
            }
        })
    }

    clear() {
        this.formStore.clear()
    }

    refresh() {
        this.formStore.formik.setValues(this.formStore.formik.initialValues)
    }

    submit() {
        this.formStore.submit()
    }

}

const useDynamicForm = (props: DFIterface) => {
    // Toaster for messages
    const toaster = useToaster();
    const [dfStore] = useState(() => {
        return new DynamicFormStore({...props, toaster})
    });
    let formik = useFormik(dfStore.formStore.init());
    dfStore.formStore.setFormik(formik);
    return [dfStore]
};

export {DynamicFormStore, useDynamicForm}