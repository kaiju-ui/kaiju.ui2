import * as React from "react";
import {Button, Sidenav, Stack} from "rsuite";
import {MenuIcon, OopenMenuIcon} from "@kaiju.ui2/icons";

const Header = ({title, expanded, setExpanded}: any) => {
    return (
        <Sidenav.Header style={{paddingTop: 10, paddingBottom: 10}}>
            {expanded &&
            <Stack
                spacing={2}
                direction={'row'}
                alignItems={'center'}
                justifyContent={'space-between'}
                style={{
                    paddingLeft: 20,

                }}
            >
                <div className={'fs-16'}>
                    {title}
                </div>
                <Button size="md" onClick={() => {
                    setExpanded(false)
                }} className={"expand-button"}>
                    <MenuIcon fill={'currentColor'} height={20}/>
                </Button>
            </Stack>
            }
            {!expanded &&
                <Button size="md" onClick={() => {
                    setExpanded(true)
                }} className={"expand-button"} style={{paddingLeft: 20}}>
                    <OopenMenuIcon fill={'currentColor'} height={20}/>
                </Button>

            }
        </Sidenav.Header>
    )
};

export default Header;