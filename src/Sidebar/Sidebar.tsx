import * as React from "react";
import { Nav, Sidenav, Divider, Sidebar as RSidebar } from "rsuite";
import { useNavigate } from "react-router-dom";
import { useMatches } from "react-router-dom";
import Header from "./header";

interface SidebarInterface {
    header: string;
    expanded: boolean;
    setExpanded: any;
    menuList: any[];
}

const Sidebar = ({ expanded, setExpanded, header, menuList }: SidebarInterface) => {
    // const [activeKey, setActiveKey] = React.useState('');
    const navigate = useNavigate();
    const matches = useMatches();

    React.useEffect(() => {
        const updateWindowDimensions = () => {
            console.log(window.innerWidth > 768, !expanded);
            if (window.innerWidth <= 768 && expanded) {
                setExpanded(false);
            } else if (window.innerWidth > 768 && !expanded) {
                setExpanded(true);
            }
        };
        window.addEventListener("resize", updateWindowDimensions);
        return () => window.removeEventListener("resize", updateWindowDimensions);
    }, [expanded]);

    return (
        <RSidebar
            collapsible
            width={expanded ? 260 : 56}
            style={{
                display: "flex",
                flexDirection: "column",
                overflowY: "auto",
            }}
        >
            <Header title={header} expanded={expanded} setExpanded={setExpanded} />
            <Sidenav expanded={expanded} defaultOpenKeys={["3"]} appearance="subtle">
                <Sidenav.Body>
                    <Nav>
                        {menuList.map((element: any) => {
                            if (element.children && !element.disable) {
                                return (
                                    <React.Fragment key={`f-key-${element.id}`}>
                                        <Divider className={"sb-divider"} />
                                        {expanded && (
                                            <Nav.Item key={`key-${element.id}`} panel className={"sb-group"}>
                                                {element.label}
                                            </Nav.Item>
                                        )}
                                        {element.children.map((child: any) => {
                                            return (
                                                <Nav.Item
                                                    eventKey={child.id}
                                                    key={`key-${child.id}`}
                                                    icon={child.icon}
                                                    onClick={() => navigate(child.link)}
                                                    active={matches.some((v) => {
                                                        return v.pathname === child.link;
                                                    })}
                                                >
                                                    {child.label}
                                                </Nav.Item>
                                            );
                                        })}
                                    </React.Fragment>
                                );
                            } else if (!element.disable) {
                                return (
                                    <Nav.Item
                                        eventKey={element.id}
                                        icon={element.icon}
                                        key={`key-${element.id}`}
                                        onClick={() => navigate(element.link)}
                                        active={matches.some((v) => {
                                            return v.pathname === element.link;
                                        })}
                                    >
                                        {element.label}
                                    </Nav.Item>
                                );
                            }
                        })}
                    </Nav>
                </Sidenav.Body>
            </Sidenav>
        </RSidebar>
    );
};

export {Sidebar};
