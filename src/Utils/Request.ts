const RequestConfs: {[key: string]: any } = {
    method: "POST",
    path: '/public/rpc',
    headers: {'Content-Type': 'application/json'}
}

interface RequestInterface {
    (method: string, params?: any): Promise<any>;
}

const Request: RequestInterface = async (method, params): Promise<any> => {
    // request('User.settings', {'id': 'user id'}).then((data) => {console.log(data)})

    const requestOptions = {
        method: RequestConfs.method,
        headers: RequestConfs.headers,
        body: JSON.stringify({
            method: method,
            params: params
        })
    };
    return await fetch(RequestConfs.path, requestOptions)
        .then((response: Response) => {
            const _data: Promise<any> = response.json()
            return _data
        });
};

// export default Request
export {Request, RequestConfs}
