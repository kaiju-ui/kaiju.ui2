import React from 'react'
import {Nav} from 'rsuite';

/*
[
    {
        id: 'some tab id,
        label: 'Tab label',
        body: ReactComponent,
        isActive: true/
    }
]

 
*/

interface TabInterface {
    id: string
    label:  React.ReactElement | string | React.ReactNode
    body: React.ReactNode

}

interface TabsInterface {
    activeKey?: string,
    tabs: Array<TabInterface>
    availableKeys?: string[]
}


const TabComponent:React.FC<{activeKey?: string, tabs: Array<TabInterface>, availableKeys?: string[]}> = ({activeKey, tabs, availableKeys}) => {
    if (!activeKey) {
        activeKey = tabs[0]?.id
    }

    const [currentActiveKey, setActiveKey] = React.useState<string>(activeKey);

    const renderLabel = (label: React.ReactElement | string | React.ReactNode) => {
        if (typeof label == "string") {
            return <>{label}</>
        } else {
            return <>{label}</>
        }
    };

    return (
        <>
            <Nav onSelect={setActiveKey} activeKey={currentActiveKey} appearance={'subtle'}>
                {tabs.map((tab: TabInterface) => {
                    return <>
                        {
                            ((!availableKeys || availableKeys.includes(tab.id)) && tab.body) &&
                        <Nav.Item key={tab.id} eventKey={tab.id}>
                            {renderLabel(tab.label)}
                        </Nav.Item>
                        }
                    </>
                })}
            </Nav>
            {tabs.map((tab: TabInterface) => {
                return (
                    <>
                        {((!availableKeys || availableKeys.includes(tab.id)) && tab.body && tab.id === currentActiveKey) &&
                        <div className={'pt-3'} key={tab.id}>
                            {tab.body}
                        </div>
                        }
                    </>
                )
            })}
        </>

    )
};

export {TabComponent};
export type {TabInterface, TabsInterface}