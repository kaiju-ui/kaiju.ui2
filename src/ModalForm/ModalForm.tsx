import React, {useState} from 'react'
import {Modal, Button} from 'rsuite';
import i18n from "i18next";
import {Form as FormComponent} from "../Form";
import {DynamicFormStore} from "../DynamicForm";

interface FormInterface {
    disableGroup?: boolean;
    loadRequest: any;
    loadExtraParams?: any;
    saveRequest: any;
    successCallback: any;
}

interface ModalInterface {
    open: any,
    handleClose: any,
    children?: any,
    title?: any,
    description?: string,
    form: FormInterface
}

const ModalForm: React.FC<ModalInterface> = ({open, title, handleClose, description, form}) => {

    // for store state
    const [dfStore] = useState(() => {
        return new DynamicFormStore(form)
    });

    // Create store on modal enter
    const handleEntered = () => {
        dfStore.load()
    };
    // remove store from state
    const onExited = () => {
        dfStore.clear()
    };

    return (
        <Modal backdrop="static" role="alertdialog" open={open} onClose={handleClose} size="md"
               onEntered={handleEntered} onExited={onExited}>
            <Modal.Header className={"pl-3 pr-3"}>
                {title &&
                <h3>
                    {title}
                </h3>
                }
                {description &&
                <div className={"pt-3"}>{description}</div>
                }

            </Modal.Header>
            <Modal.Body>
                <FormComponent store={dfStore.formStore} key={"form.modal"} className={"pl-3 pr-3"}/>
            </Modal.Body>
            <Modal.Footer className={"p-3"}>
                <Button onClick={() => {
                    dfStore.submit()
                }} appearance="primary">
                    {i18n.t('label.save')}
                </Button>
                <Button onClick={handleClose} appearance="subtle">
                    {i18n.t('label.cancel')}
                </Button>
            </Modal.Footer>
        </Modal>
    )
};

export default ModalForm;

