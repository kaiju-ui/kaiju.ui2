import {DynamicFormStore} from '../DynamicForm/store'

const ModalFormStore = (props: any) => {
    const formStore = new DynamicFormStore(props);
    return {formStore}
};

export default ModalFormStore;
