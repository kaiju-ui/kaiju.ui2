import React from 'react'
import {action, makeObservable, observable} from "mobx";
import {Request} from '../Utils'


interface RequestInterface {
    method: string,
    params?: { [key: string]: any }
}

interface TableInterface {
    requestFunc?: any,
    request: RequestInterface,
    delete?: RequestInterface,
    rowClick?: (rowData: any, dataKey: string, event?: any) => void,
    beforeFetchCallback?: (params: any) => void,
    actionComponent?: (rowData: any, store?: any) => React.ReactNode | undefined;
    resizable?: boolean
    rowKey?: string
    query?: string | undefined
    cellID?: { [key: string]: any }
    cellKind?: { [key: string]: any }
    perPage?: number
    disablePagination?: boolean

}

interface ResponseInterface {
    fields: string[]
    header: any[]
    data: any[]
    pagination: { page: number, pages: number }
    count: number
}

class TableStore {
    requestFunc: any = Request
    count: number = 0;
    fields: any[] = [];
    header: any[] = [];
    data: any[] = [];
    fetch: boolean = false;
    disablePagination: boolean = false;
    currentPage: number = 1;
    perPage: number = 24;
    rowKey: string;
    cellID?: { [key: string]: any } = {};
    cellKind?: { [key: string]: any } = {};
    resizable: boolean = false;
    query: string | undefined;
    request: RequestInterface;
    delete?: RequestInterface | undefined;
    rowClick?: ((rowData: any, dataKey: string, event?: any) => void) | undefined;
    beforeFetchCallback?: ((params: any) => void) | undefined;
    actionComponent?: ((rowData: any, store?: any) => React.ReactNode) | undefined;

    constructor(props: TableInterface) {
        this.request = props.request;
        this.requestFunc = props.requestFunc || Request;
        this.currentPage = this.request.params?.page || this.currentPage;
        this.query = props.query || undefined;
        makeObservable(this, {
            setData: action.bound,
            fetchData: action.bound,
            updateDataById: action.bound,
            data: observable.shallow,
            currentPage: observable,
            // fetch: observable,
            fields: observable,
            header: observable.shallow,
        });

        this.fetch = false;
        this.cellID = props.cellID || this.cellID;
        this.disablePagination = props.disablePagination || this.disablePagination;
        this.cellKind = props.cellKind || this.cellKind;
        this.rowKey = props.rowKey || 'id';
        this.actionComponent = props.actionComponent;
        this.rowClick = props.rowClick;
        this.beforeFetchCallback = props.beforeFetchCallback;
        this.delete = props.delete;
        this.resizable = props.resizable || false;
        this.perPage = props.perPage || this.perPage;
        this.fetchData()
    }

    setData({data, pagination, count, header}: ResponseInterface) {
        // this.fields = fields;
        this.data = data;
        this.count = count;
        this.header = header;
        this.currentPage = pagination['page'];
    }

    setQuery(query: string) {
        this.query = query
    }

    updateDataById(id: any, data: any) {
        this.data.forEach((el: any, idx: number) => {
            if (el.id === id) {
                this.data[idx] = {...el, ...data}
            }
        })
    }

    fetchData(page?: number) {
        if (!this.fetch) {
            this.fetch = true;
            const params = {
                ...this.request.params,
                query: this.query,
                page: page || this.currentPage,
                per_page: this.perPage,
            };
            if (this.beforeFetchCallback) {
                this.beforeFetchCallback({
                    ...this.request.params,
                    query: this.query,
                    page: page || this.currentPage,
                })
            }
            this.requestFunc(this.request.method, params).then((response: any) => {
                if (response.result) {
                    this.setData(response.result);
                    this.fetch = false;
                }
                this.fetch = false;
            })
        }
    }
}


interface SortableInterface extends TableInterface {
    updateSort: RequestInterface;
}


class SortableTableStore extends TableStore {
    updateSort: RequestInterface;

    constructor(props: SortableInterface) {
        super(props);
        this.updateSort = props.updateSort
    }

    handleDragRow(sourceId: any, targetId: any) {
        const params = {
            ...this.updateSort.params,
            id: sourceId.id,
            target: targetId.id,
            target_sort: targetId.value
        };
        Request(this.updateSort.method, params).then(response => {
            if (response.error) {

            } else {
                this.fetchData()
            }
            console.log("RESPONSE", response)
        })
    };
}

// const useTableStore = (props: TableInterface) => {
//     const tableStore = new TableStore(props);
//     return {tableStore}
// };

export {TableStore, SortableTableStore};
