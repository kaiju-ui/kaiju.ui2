import React from 'react'
import i18n from 'i18next'
import {Observer} from 'mobx-react'
import {Table as RTable, Panel, Pagination, Placeholder} from 'rsuite';
import BaseCell from './cell'

const {Column, HeaderCell, Cell} = RTable;

interface StoreInterface {
    header: any[];
    fields: any[];
    data: any[];
    fetch: boolean;
    disablePagination: boolean;
    rowKey: string;
    currentPage: number;
    fetchData: (page?: any) => void;
    perPage: number;
    count: number;
    actionComponent?: (rowData: any, store: any) => React.ReactNode | undefined;
    rowClick?: (rowData: any, dataKey: string, event?: any) => void;
}

const ActionCell = ({store, rowData, ...props}: any) => {
    return <Cell {...props} style={{cursor: "pointer"}}>
        {store.actionComponent ? store.actionComponent(rowData, store) : <></>}
    </Cell>
};


const Table: React.FC<{store: StoreInterface}> = ({store}) => {
    return (
        <Panel className={"base-table"} style={{position: "relative"}}>
            <Observer>{
                () => (
                    <>
                        {store.header.length > 0 &&
                            <>
                                {!store.disablePagination &&
                                    <Pagination
                                        prev
                                        next
                                        ellipsis
                                        boundaryLinks
                                        locale={{
                                            total: `${i18n.t('label.total').toString()}: ${store.count}`
                                        }}
                                        maxButtons={3}
                                        size="md"
                                        layout={['total', 'pager']}
                                        total={store.count}
                                        limit={store.perPage}
                                        activePage={store.currentPage}
                                        onChangePage={(page: number) => {
                                            store.fetchData(page)
                                        }}
                                        className={"center-pagination pb-3"}
                                    />
                                }

                                <RTable
                                    data={store.data}
                                    // minHeight={600}
                                    loading={store.fetch}
                                    // rowHeight={100}
                                    // rowExpandedHeight={500}
                                    // virtualized
                                    height={600}
                                    autoHeight
                                    affixHeader
                                    affixHorizontalScrollbar={20}
                                    // renderRow={(children?: any, rowData?: any) => {
                                    //     console.log("children:", children)
                                    //     console.log("rowData:", rowData)
                                    //     if (!rowData){
                                    //         return children
                                    //     }
                                    //     return <div>{123412341234}</div>
                                    // }}
                                >
                                    {store.header.map((el: any) => {
                                        return (
                                            <Column key={`Column-${el.id}`} minWidth={250} flexGrow={1} >
                                                <HeaderCell><b>{el.label ? el.label : el.label_key ? i18n.t(el.label_key) : el.id}</b></HeaderCell>
                                                <BaseCell dataKey={el.id} rowClick={store.rowClick} store={store}/>
                                            </Column>
                                        )
                                    })}
                                    {store.actionComponent &&
                                        <Column width={100} flexGrow={1}>
                                            <HeaderCell>...</HeaderCell>
                                            <ActionCell store={store}/>
                                        </Column>
                                    }
                                </RTable>
                            </>
                        }
                        {store.header.length === 0 &&
                            <Placeholder.Grid rows={10} columns={4} active/>
                        }
                    </>
                )}</Observer>
        </Panel>
    );
};

export {Table};
export type {StoreInterface}
