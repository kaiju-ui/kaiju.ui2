import React from "react";
import './store'
import {CheckIcon} from "@kaiju.ui2/icons";
import {Table} from 'rsuite';

const {Cell} = Table;

interface CellInterface {
    rowData?: { [key: string]: any };
    dataKey: string;
    width?: any;
    store: any;
    rowClick?: (rowData: any, dataKey: string, event?: any) => void | undefined
}

const cellKinds: { [key: string]: any } = {
    boolean: ({value}: any) => {
        if (value) {
            return <CheckIcon height={"1.2rem"}/>
        } else {
            return <></>
        }
    }
};

const cellId: { [key: string]: any } = {};


const CellConstructor = ({rowData, dataKey, rowClick, store, ...props}: CellInterface) => {
    const kind = rowData?.[dataKey]?.kind;
    const Viewer = store.cellID[dataKey] || store.cellKind[kind] || cellId[dataKey] || cellKinds[kind];

    return (
        <Cell {...props} style={{cursor: "pointer"}} onClick={(event: any) => {
            const selection = window.getSelection();
            if (rowClick && selection?.type !== 'Range') {
                rowClick(rowData, dataKey, event)
            }
        }}>
            {Viewer && <Viewer value={rowData?.[dataKey]?.value} rowData={rowData}/>}
            {!Viewer && rowData?.[dataKey]?.value?.toString()}
        </Cell>
        // <div  style={{width: width}} className={"tb-cell"}>
        //     {Viewer && <Viewer value={rowData?.[dataKey]?.value} rowData={rowData}/>}
        //     {!Viewer && rowData?.[dataKey]?.value?.toString()}
        // </div>
    )
};

export default CellConstructor;
