import {Table as BaseTable} from './Table'
import {TableStore, SortableTableStore} from './store'
const Table = Object.assign(BaseTable, {TableStore, SortableTableStore})
export {Table}