import React from 'react'
import {Badge, Form, Stack} from "rsuite";

interface Interface {
    label: any,
    required?: boolean,
    changed?: boolean,
    children?: any,

}

export default ({label, required, changed, children}: Interface) => (
    <Form.ControlLabel>
        <Stack justifyContent={"space-between"}>
            <div>
                {required && <span style={{color: "red"}} className={"fs-16"}>*</span>} {label} {changed && <Badge color="yellow"/>}
            </div>
            <div>
                {children && children}
            </div>
        </Stack>
    </Form.ControlLabel>
)

