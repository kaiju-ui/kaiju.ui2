import StringStore from './FormFields/string'
import PasswordStore from './FormFields/password'
import Boolean from './FormFields/boolean'
import IntegerStore from './FormFields/integer'
import SelectStore from './FormFields/select'
import DecimalStore from './FormFields/decimal'
import MultiSelectStore from './FormFields/multi_select'
import NestedStore from './FormFields/nested'
import NestedListStore from './FormFields/nested_list'
import TextStore from './FormFields/text'
import JsonObjectStore from './FormFields/json'
import TextBlock from './FormFields/textBlock'
import TagStore from './FormFields/tag'

const kindMap: {[key: string]: any} = {
    string: StringStore,
    text: TextStore,
    password: PasswordStore,
    boolean: Boolean,
    integer: IntegerStore,
    select: SelectStore,
    decimal: DecimalStore,
    multiselect: MultiSelectStore,
    multi_select: MultiSelectStore,
    nested: NestedStore,
    nested_list: NestedListStore,
    json: JsonObjectStore,
    json_object: JsonObjectStore,
    text_block: TextBlock,
    tag: TagStore,
};

const idMap: {[key: string]: any} = {};

export {idMap, kindMap};
