import React from "react";
import {observable, makeObservable, action} from "mobx";
import {useFormik} from 'formik';
import {idMap, kindMap} from './fields';
import * as Yup from 'yup';
import {Form} from "./index";

/*
fields: [
    {
        "id": "Group.general",
        "fields": [
            {
                "id": "id",
                "default": null,
                "disabled": false,
                "kind": "string",
                "required": true,
                "dependence": null,
                "regex": {
                    "pattern": "^[a-zA-Z0-9_]+$",
                    "msg": "Regex.key"
                },
                "behavior": null
            },
            {
                "id": "kind",
                "default": null,
                "disabled": true,
                "kind": "select",
                "required": true,
                "dependence": null,
                "options_handler": "AttributeKind.load",
                "params": null,
                "value": "string_list"
            },
            {
                "id": "group",
                "default": [],
                "disabled": false,
                "kind": "select",
                "required": false,
                "dependence": null,
                "options_handler": "AttributeGroup.load",
                "params": {
                    "entity": "e0e4143c0f6d5f32733e4dc0b2963882"
                }
            },
            {
                "id": "projection",
                "default": [],
                "disabled": false,
                "kind": "multiselect",
                "required": false,
                "dependence": null,
                "options_handler": "Projection.load",
                "params": null
            },
            {
                "id": "use_in_grid",
                "default": true,
                "disabled": false,
                "kind": "boolean",
                "required": false,
                "dependence": null
            },
            {
                "id": "read_only",
                "default": false,
                "disabled": false,
                "kind": "boolean",
                "required": false,
                "dependence": null
            }
        ]
    },
    {
        "id": "Group.validators",
        "fields": [
            {
                "id": "min_characters",
                "default": null,
                "disabled": false,
                "kind": "integer",
                "required": false,
                "dependence": null,
                "negative_value": false
            },
            {
                "key": "max_characters",
                "default": null,
                "disabled": false,
                "kind": "integer",
                "required": false,
                "dependence": null,
                "negative_value": false
            }
        ]
    },
    {
        "id": "Group.search",
        "fields": [
            {
                "key": "use_in_filtering",
                "default": false,
                "disabled": false,
                "kind": "boolean",
                "required": false,
                "dependence": null
            }
        ]
    }
]

 */

interface FSInterface {
    fields: any[],
    disableGroups?: boolean,
    onSubmit: any
    loadExtraParams?: { [key: string]: any } | undefined
}


class FormStore {
    fieldMap: any = {};
    groups: any[] = [];
    idMap: any = idMap;
    kindMap: any = kindMap;
    disableGroups: boolean = false;
    formik: any;
    initialValues: any = {};
    validators: any = {};
    _onSubmit: any;
    loadExtraParams?: any;

    constructor(props: FSInterface) {
        makeObservable(this, {
            groups: observable,
            initFields: action.bound,
            clear: action.bound,
            reset: action.bound
        });
        const {fields, disableGroups, onSubmit, loadExtraParams} = props;
        this.loadExtraParams = loadExtraParams;
        this.initFields(fields);
        this.disableGroups = disableGroups || this.disableGroups;
        this._onSubmit = onSubmit;
    }

    setFormik(formik: any) {
        this.formik = formik;
    }

    init = () => {
        return {
            initialValues: this.initialValues,
            validationSchema: Object.keys(this.validators).length > 0 ? Yup.object().shape(this.validators) : undefined,
            onSubmit: this.onSubmit,
        }
    };

    initFields(fields: any[]) {
        const valStructure: { [key: string]: any } = {};
        fields.forEach((group) => {
            const _fieldsInGroup: any[] = [];

            group.fields.map((field: any) => {
                valStructure[field.id] = field;
                const fStore = this.idMap[field.id] ? this.idMap[field.id] : this.kindMap[field.kind] ? this.kindMap[field.kind] : undefined;
                if (fStore) {

                    const _store = new fStore({form: this, ...field, loadExtraParams: this.loadExtraParams});
                    this.fieldMap[field.id] = _store;
                    _fieldsInGroup.push(_store);
                    this.validators[field.id] = _store.validator;
                    this.initialValues[field.id] = _store.value
                }
            });

            this.groups.push({
                id: group.id,
                label: group.label,
                fields: _fieldsInGroup
            })
        });
        return valStructure
    }

    clear() {
        this.groups = [];
        this.fieldMap = {};
        this.validators = {};
        this.initialValues = {}
    }
    reset(data: Object){
        this.formik.resetForm(data)
    }

    onSubmit = async (values: any, formitState: any) => {
        const changedValues = this.getChangedValues({...values}, this.initialValues)
        this._onSubmit(values, formitState, changedValues)
    };

    submit = (event: any) => {
        this.formik.submitForm(event)
    };
    getChangedValues = (values: { [key: string]: any }, initialValues: { [key: string]: any }) => {
        return Object
            .entries(values)
            .reduce((acc: { [key: string]: any }, [key, value]) => {
                const hasChanged = initialValues[key] !== value;

                if (hasChanged) {
                    acc[key] = value
                }

                return acc
            }, {})
    }
}

function useFormStore(settings: FSInterface) {
    const [store] = React.useState(()=> {
        return new Form.FormStore(settings)
    })

    let formik = useFormik(store.init());
    store.setFormik(formik);
    return store;
}


export default FormStore
export {useFormStore}
