import * as Yup from 'yup';
import StringStore from "./string";


class PasswordStore extends StringStore {
    validator: any;
    type = 'password';
    
    constructor(props: any) {
        super(props);
        this.validator = Yup.string();
        this.makeValidator(props)
    }

}

export default PasswordStore;
