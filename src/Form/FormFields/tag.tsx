import {Form} from "rsuite";
import {TagInput} from 'rsuite';
import React from "react";
import * as Yup from 'yup';
import FieldStore from './base';
import {Error} from '../error';
import {Helper} from "../utils";
import Label from "../label";
class TagStore extends FieldStore {
    validator: any;
    constructor(props: any) {
        super(props);
        this.defaultValue = props.default || [];
        this.value = props.value || this.defaultValue;
        this.validator = Yup.array().nullable();
        this.makeValidator(props)
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        const values = props.values || props.formik.values;
        const _id = this._id(props);

        return (
            <>
                <Form.Group>
                    <Label label={this.label} required={this.props.required}/>
                    <div style={{display: "flex"}}>
                        <div className={"rs-form-control rs-form-control-wrapper"}>
                            <TagInput
                                style={{width: '100%'}}
                                onCreate={(value) => {
                                    props.formik.setFieldValue(_id, value)
                                }}
                                disabled={this.disabled}
                                id={this.id}
                                name={this.id}
                                defaultValue={values[this.id]}
                                // data={[]}
                                onBlur={props.formik.handleBlur}
                                trigger={"Enter"}
                            />
                            <Error message={errors[this.id]}/>
                        </div>
                        <Helper comp={this} className={"fs-16"}/>
                    </div>
                </Form.Group>
            </>
        )
    }

}

export default TagStore;
