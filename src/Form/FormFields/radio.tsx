import {Form} from "rsuite";
import React from "react";


const BaseComponent = () => {
    return (
        <Form.Group>
            <Form.ControlLabel>BaseComponent</Form.ControlLabel>

        </Form.Group>
    )
};


class BaseStore {
    component: React.FC = BaseComponent;
}

export default BaseStore;
