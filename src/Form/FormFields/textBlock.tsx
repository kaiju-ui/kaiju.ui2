import {Form} from "rsuite";
import React from "react";
import FieldStore from './base';
import i18n from "i18next";

class TextBlockStore extends FieldStore {
    validator: any;
    text: string | undefined | null;
    textKey: string | undefined | null;

    constructor(props: any) {
        super(props);
        this.text = props.text;
        this.textKey = props.text_key;
        this.validator = undefined;
    }

    Component: React.FC = () => {
        return (
            <>
                <Form.Group style={{marginTop: "-20px"}}>
                    <div style={{display: "flex"}}>
                        <div className={"rs-form-control rs-form-control-wrapper"} style={{ whiteSpace: "pre-line" }}>
                            {/*{!!this.textKey && this.textKey} */}
                            {!!this.textKey && i18n.t(this.textKey)}

                        </div>
                    </div>
                </Form.Group>
            </>
        )
    }

}

export default TextBlockStore;
