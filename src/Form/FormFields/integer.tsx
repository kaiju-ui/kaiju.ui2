import {Form} from "rsuite";
import {InputNumber} from 'rsuite';
import React from "react";
import * as Yup from 'yup';
import FieldStore, {FieldStoreInterface} from './base';
import {Error} from '../error';
import {Helper} from "../utils";
import Label from "../label";
import ViewCondition from "../ViewCondition";

interface IntegerInterface extends FieldStoreInterface {
    max?: number | undefined
    min?: number | undefined
    step?: number | undefined
}


class IntegerStore extends FieldStore {
    validator: any;
    max: any;
    min: any;
    step: any;

    constructor(props: IntegerInterface) {
        super(props);
        this.validator = Yup.number().nullable();
        this.makeValidator(props);
        this.max = props.max;
        this.min = props.min;
        this.step = props.step
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        const values = props.values || props.formik.values;
        const _id = this._id(props);
        return (
            <ViewCondition values={values} viewCondition={this.props.view_condition}>
                <Form.Group>
                    <Label label={this.label} required={this.props.required}/>
                    <div style={{display: "flex"}}>
                        <div className={"rs-form-control rs-form-control-wrapper"}>
                            <InputNumber
                                id={_id}
                                name={_id}
                                disabled={this.disabled}
                                placeholder={this.placeholder}
                                onChange={(val: any) => {
                                    props.formik.setFieldValue(_id, val? parseInt(val): val)
                                }}
                                value={values[this.id]}
                                onBlur={props.formik.handleBlur}
                                min={this.min}
                                max={this.max}
                            />
                            <Error message={errors[this.id]}/>

                        </div>
                        <Helper comp={this} className={"fs-16"}/>
                    </div>
                </Form.Group>
            </ViewCondition>
        )
    }

}

export default IntegerStore;
