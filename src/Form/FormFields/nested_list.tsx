import {Form, IconButton, Stack} from "rsuite";
import React, {useCallback} from "react";
import * as Yup from 'yup';
import FieldStore, {FieldStoreInterface} from './base';
import Label from "../label";
import ViewCondition from "../ViewCondition";
import {AddIcon, CloseIcon} from "@kaiju.ui2/icons";
import i18n from "i18next";
import {FieldArray} from 'formik';
import {Row} from "../../DndList";
import {DndProvider} from 'react-dnd'
import {HTML5Backend} from 'react-dnd-html5-backend'

interface Interface extends FieldStoreInterface {
    fields: any[]

}

class NestedListStore extends FieldStore {
    validator: any;
    fields: any[] = [];
    value: any[];

    constructor(props: Interface) {
        super(props);
        this.value = Array.isArray(props.value) ? props.value : [];
        this.validator = Yup.array().nullable();
        this.initNested(props.fields);
        this.makeValidator(props);

    }

    initNested(fields: any[]) {
        const _validators: { [key: string]: any } = {};

        fields.map((field: any) => {
            const fStore = this.form.kindMap[field.kind] ? this.form.kindMap[field.kind] : undefined;
            if (fStore) {
                const _store = new fStore({
                    form: this.form,
                    parentId: this.id,
                    ...field,
                    loadExtraParams: this.loadExtraParams
                });
                this.fields.push(_store);
                _validators[field.id] = _store.validator;
                this.value.forEach(val => {
                    if (!(field.id in val)) {
                        val[field.id] = _store.value
                    }
                })
            }
        });
        this.validator = this.validator.of(
            Yup.object().shape(
                _validators
            )
        )
    }

    emptyValue = () => {
        const resp: { [key: string]: any } = {};
        this.fields.forEach((val => {
            resp[val.id] = val.value;
        }));
        return resp
    };

    Component: React.FC = (props: any) => {
        const [state, setState] = React.useState((new Date()).toISOString());
        const moveCard = useCallback((move: any, dragIndex: number, hoverIndex: number) => {
            console.log("\n>>", dragIndex, hoverIndex)
            console.log("\n")
            move(dragIndex, hoverIndex)

        }, [])

        return (
            <ViewCondition values={props.formik.values} viewCondition={this.props.view_condition}>
                <Form.Group>
                    <Label label={this.label} required={this.props.required}/>
                    <FieldArray
                        name={this.id}
                        render={(arrayHelpers: any) => (
                            <DndProvider backend={HTML5Backend}>
                                {(arrayHelpers.form.values[this.id] && arrayHelpers.form.values[this.id].length > 0) && arrayHelpers.form.values[this.id].map((values: any, index: number) => {
                                    return (
                                        <Row key={`${state}${index}`} index={index}
                                             moveCard={(dragIndex: number, hoverIndex: number) => {
                                                 // setState((new Date()).toISOString())()()
                                                 moveCard(arrayHelpers.move, dragIndex, hoverIndex)
                                                 setState((new Date()).toISOString())

                                             }}>
                                            <div className={"border rowBlock mb-3"} style={{display: "flex"}}>

                                                <div
                                                    className={"rs-form-control rs-form-control-wrapper flex-nested"}>
                                                    {this.fields.map((fieldStore: any) => {
                                                        return <fieldStore.Component
                                                            key={`form-field-nested-${fieldStore._id()}`}
                                                            formik={arrayHelpers.form}
                                                            values={values}
                                                            index={index}
                                                            errors={props.formik.errors[this.id]?.[index] || {}}/>
                                                    })}
                                                </div>
                                                <a className={"pointer"} style={{width: 10}} onClick={() => {
                                                    arrayHelpers.remove(index)
                                                    setState((new Date()).toISOString())
                                                }}>
                                                    <CloseIcon size={18}/>
                                                </a>
                                            </div>
                                        </Row>
                                    )
                                })}
                                {(!arrayHelpers.form.values[this.id] || arrayHelpers.form.values[this.id].length === 0) &&
                                <div className={"no-data"}>
                                    {i18n.t("label.no_data")}
                                </div>

                                }

                                <Stack
                                    spacing={1}
                                    justifyContent={'center'}
                                >
                                    <IconButton
                                        onClick={() => {
                                            arrayHelpers.push(this.emptyValue())
                                        }}
                                        appearance="primary"
                                        className={"fs-16"}
                                        icon={<AddIcon height={"1em"} fill={"currentColor"}/>}
                                    >
                                        {i18n.t("label.add")}
                                    </IconButton>
                                </Stack>
                            </DndProvider>
                        )}/>
                </Form.Group>
            </ViewCondition>
        )
    }

}

export default NestedListStore;
