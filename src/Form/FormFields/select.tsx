import {Form} from "rsuite";
import React from "react";
import * as Yup from 'yup';
import FieldStore, {FieldStoreInterface} from './base';
import {Error} from '../error';
import Label from '../label'

import {Helper} from "../utils";
import {Select} from "../../Fields";

interface Interface extends FieldStoreInterface {
    options_handler: string,
    params: any,
}


class SelectStore extends FieldStore {
    validator: any;
    optionHandler: string;
    params: any;

    constructor(props: Interface) {
        super(props);
        this.optionHandler = props.options_handler;
        this.params = props.params;
        this.defaultValue = props.default || "";
        this.validator = Yup.string().nullable();
        this.makeValidator(props)
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        const values = props.values || props.formik.values;
        const _id = this._id(props);

        return (
            <>
                <Form.Group>
                    <Label label={this.label} required={this.props.required}/>
                    <div style={{display: "flex"}}>
                        <div className={"rs-form-control rs-form-control-wrapper"}>
                            <Select
                                onChange={(val: any) => {
                                    props.formik.setFieldValue(_id, val)
                                }}
                                disabled={this.disabled}
                                value={values[this.id]}
                                optionHandler={this.optionHandler}
                                params={{...this.params, ...this.loadExtraParams}}
                                defaultValue={this.defaultValue}
                            />
                            <Error message={errors[this.id]}/>
                        </div>
                        <Helper comp={this} className={"fs-16"}/>
                    </div>
                </Form.Group>
            </>
        )
    }

}

export default SelectStore;
