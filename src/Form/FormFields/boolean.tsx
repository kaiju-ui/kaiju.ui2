import {Form} from "rsuite";
import React from "react";
import * as Yup from 'yup';
import FieldStore from './base';
import {Toggle} from 'rsuite';
import CheckIcon from '@rsuite/icons/Check';
import CloseIcon from '@rsuite/icons/Close';
import {Helper} from "../utils";
import ViewCondition from '../ViewCondition'

class BooleanStore extends FieldStore {
    validator: any;

    constructor(props: any) {
        super(props);
        this.value = typeof props.value === 'boolean' ? props.value : this.defaultValue;
        this.validator = Yup.boolean().nullable();
        this.makeValidator(props);
    }

    Component: React.FC = (props: any) => {
        const values = props.values || props.formik.values;
        const _id = this._id(props)
        return (
            <ViewCondition values={props.formik.values} viewCondition={this.props.view_condition}>
                <Form.Group>
                    <div style={{display: "flex"}}>
                        <div className={"rs-form-control rs-form-control-wrapper"}>
                            <Toggle
                                checkedChildren={<CheckIcon/>}
                                unCheckedChildren={<CloseIcon/>}
                                size={'lg'}
                                onChange={(val: any) => {
                                    props.formik.setFieldValue(_id, val)
                                }}
                                defaultChecked={values[this.id]}
                            ></Toggle>
                            <label className={"pl-3"}>{this.label}</label>
                            <Helper comp={this} className={"fs-16"}/>
                        </div>
                    </div>
                </Form.Group>
            </ViewCondition>
        )
    }
}

export default BooleanStore;
