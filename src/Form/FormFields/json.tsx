import {Form} from "rsuite";
import React from "react";
import ReactJson from 'react-json-view'
import FieldStore from './base';
import {Helper} from "../utils";
import Label from "../label";


class JsonObjectStore extends FieldStore {
    validator: any;
    readOnly: boolean = false;

    constructor(props: any) {
        super(props);
        // this.defaultValue = props.default || "";
        this.validator = undefined
        this.defaultValue = props.field_type == 'list' ? [] : {}
        this.readOnly = props.read_only || false
        // this.makeValidator(props)
    }

    Component: React.FC = (props: any) => {
        // const errors = props.errors || props.formik.errors;
        const values = props.values || props.formik.values;
        const _id = this._id(props);
        const _value = values[this.id] || this.defaultValue;
        let jsonProps: {[key: string]: any} = {}

        if (!this.readOnly) {
            jsonProps["onAdd"] = (e: any) => {
                    props.formik.setFieldValue(_id, e.updated_src)
            };

            jsonProps["onEdit"] = (e: any) => {
                    props.formik.setFieldValue(_id, e.updated_src)
            };
            jsonProps["onDelete"] = (e: any) => {
                    props.formik.setFieldValue(_id, e.updated_src)
            };

        }

        return (
            <>
                <Form.Group>
                    <Label label={this.label} required={this.props.required}/>
                    <div style={{display: "flex"}}>
                        <div className={"rs-form-control rs-form-control-wrapper"}>
                            <ReactJson
                                name={null}
                                src={_value}
                                {...jsonProps}
                            />
                            {/*<Error message={errors[this.id]}/>*/}
                        </div>
                        <Helper comp={this} className={"fs-16"}/>
                    </div>
                </Form.Group>
            </>
        )
    }

}

export default JsonObjectStore;
