import {Form} from "rsuite";
import {Input, MaskedInput} from 'rsuite';
import React from "react";
import * as Yup from 'yup';
import FieldStore from './base';
import {Error} from '../error';
import {Helper} from "../utils";
import Label from "../label";

class StringStore extends FieldStore {
    validator: any;
    type = 'text';
    mask?: any[] | undefined;

    constructor(props: any) {
        super(props);
        this.defaultValue = props.default || "";
        this.validator = Yup.string().nullable();
        this.makeValidator(props);
        this.mask = props.mask
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        const values = props.values || props.formik.values;
        const _id = this._id(props);
        const INPT = this.mask ? MaskedInput : Input;
        let defaultLen: number = 0;

        if (typeof values[this.id] === "string") {
            defaultLen = values[this.id].length
        }
        const extraParams: {[key: string]: any} = {};
        if (this.mask){
            extraParams["placeholderChar"] = " "
        }

        const [valueLen, setValueLen] = React.useState<number>(defaultLen);
        return (
            <>
                <Form.Group>
                    <Label label={this.label} required={this.props.required}>
                        {typeof this.props.max === 'number' &&
                        <span style={{color: '#1675e0'}}>
                            {`${valueLen} / ${this.props.max}`}
                        </span>
                        }
                    </Label>
                    <div style={{display: "flex"}}>
                        <div className={"rs-form-control rs-form-control-wrapper"}>
                            <INPT
                                {...extraParams}
                                mask={this.mask}
                                id={this.id}
                                type={this.type}
                                name={this.id}
                                disabled={this.disabled}
                                placeholder={this.placeholder}
                                onChange={(val: any) => {
                                    props.formik.setFieldValue(_id, val ? val.trim() : val)
                                    if (typeof val === "string") {
                                        setValueLen(val.trim().length)
                                    } else {
                                        setValueLen(0)
                                    }
                                }}
                                value={values[this.id]}
                                onBlur={props.formik.handleBlur}
                            />
                            <Error message={errors[this.id]}/>
                        </div>
                        <Helper comp={this} className={"fs-16"}/>
                    </div>
                </Form.Group>
            </>
        )
    }

}

export default StringStore;
