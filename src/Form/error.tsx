import React from "react";
const Error: React.FC<{ message: string }> = ({message}) => {
    return (
        <>
            {message &&
            <div id={"input-2-error-message" + message}
                 role="alert"
                 aria-relevant="all"
                 className="rs-form-control-message-wrapper rs-form-error-message-wrapper rs-form-error-message-placement-bottom-end"
            >
            <span className="rs-form-error-message rs-form-error-message-show">
                <span className="rs-form-error-message-arrow"></span>
                <span className="rs-form-error-message-inner">
                    {message}
                </span>
            </span>
            </div>
            }
        </>

    )
};

export {Error};
