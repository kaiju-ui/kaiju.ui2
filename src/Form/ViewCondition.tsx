import React from "react";

export default ({values, viewCondition, children}: any) => {
    if (!viewCondition){
        return <>{children}</>
    } else {
        const eq = Object.keys(viewCondition).some((key: string) => {
            const currentValue = values[key];
            if (currentValue === viewCondition[key]){
                return true
            }
            return false
        });
        return <>{eq && children}</>
    }
};