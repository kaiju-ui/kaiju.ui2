import {Form} from "rsuite";
import i18n from "i18next";
import React from "react";

const Helper = (props: any) => {
    const {comp, className} = props;
    return (
        <>
            {
                comp.helperKey &&
                <Form.HelpText tooltip className={className}>{i18n.t(comp.helperKey)}</Form.HelpText>
            }
            {
                (comp.helperText && !comp.helperKey) &&
                <Form.HelpText tooltip className={className}>{comp.helperText}</Form.HelpText>
            }
        </>
    )
};

export {Helper}
