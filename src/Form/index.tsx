import {Form as BaseForm} from "./Form"
import * as Fields from './FormFields';
import FormStore, {useFormStore} from "./store"
import {idMap, kindMap} from './fields'
const Form = Object.assign(BaseForm, {FormStore, useFormStore, Fields, idMap, kindMap})
export {Form}