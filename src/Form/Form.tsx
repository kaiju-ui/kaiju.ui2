import React from 'react'
import {FormikProvider} from 'formik';
import {Form as RForm, Loader} from 'rsuite';
import {Observer} from 'mobx-react'


const Form: React.FC<{ store: any, children?: any, className?: any, style?: any }> = ({store, children, style, className}) => {
    return (
        <Observer>{() => (
            <FormikProvider value={store.formik}>
                <RForm fluid className={className} style={style}>
                    {
                        store.groups.length ? <>
                            {
                                store.groups.map((group: any) => {
                                    return group.fields.map((fieldStore: any) => {
                                        return <fieldStore.Component key={`form-field-${fieldStore.id}`}
                                                                     formik={store.formik} store={store}/>
                                    })
                                })
                            }
                            {children}
                        </> : <div className={"p-4 text-center"}>
                            <Loader size="lg"/>
                        </div>
                    }
                </RForm>
            </FormikProvider>
        )}
        </Observer>
    )
};
export {Form};
