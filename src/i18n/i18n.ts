import i18n from "i18next";
import {initReactI18next} from "react-i18next";

const LoadTranslations = async () => {
    await fetch('/api/translation')
        .then((response) => response.json())
        .then((data) => {
            const resources = {
                ...data
            };
            i18n.use(initReactI18next)
                .init({
                    resources,
                    lng: data.locale,
                });
        });

};

export {LoadTranslations};
