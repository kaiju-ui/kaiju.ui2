import React from 'react'
import {TagPicker} from 'rsuite';
import {Select} from './Select'

const MultiSelect = (props: any) => {
    return (
        <Select
            {...props}
            Component={TagPicker}
        />
    );
};

export {MultiSelect};
