import React from 'react'
import {SelectPicker} from 'rsuite';
import SpinnerIcon from '@rsuite/icons/legacy/Spinner';
import {Request} from '../Utils';
import i18n from 'i18next'
import {PickerHandle} from 'rsuite/Picker';

interface SelectInterface {
    onChange: any
    defaultValue?: any
    style?: any
    className?: any
    value?: any
    renderExtraFooter?: any
    renderValue?: any
    container?: any
    cleanable?: boolean | undefined
    placeholder?: string | undefined
    optionHandler?: string | undefined
    data?: any[]
    params?: any
    searchable?: boolean | undefined
    disabled?: boolean
    Component?: React.FC
}

interface requestInterface {
    hasMore: boolean,
    params: any

}

interface scrollInterface {
    isScrolled: boolean,
    fetch: boolean,
    offset: number
}

const useData = (optionHandler: any, params: any, defaultValue: string[] | undefined | null) => {
    let defaultItems: any[] = [];

    const [loading, setLoading] = React.useState(false);
    const [items, setItems] = React.useState<any[]>(defaultItems);
    const [loadDefaultState, setLoadDefaultState] = React.useState(false);

    const [requestState, setRequestState] = React.useState<requestInterface>({
        hasMore: true,
        params: {
            q: null,
            page: 1,
            ...params
        }
    });

    const [scrollState, setScrollState] = React.useState<scrollInterface>({
        isScrolled: true,
        fetch: false,
        offset: 0
    });

    const queryFetch = async (query: any) => {
        const params = {
            ...requestState.params,
            query: query || null,
            page: 1,
        };
        setRequestState({
            hasMore: true,
            params: params
        });
        await fetchData(params, true)

    };
    const fetchData = async (params?: any, replace: boolean = false) => {
        let localItems: any[] = [];

        if (optionHandler && !loading && requestState.hasMore) {
            setLoading(true);
            await Request(optionHandler, params || requestState.params).then((response: any) => {
                if (response.result?.data) {
                    const newItems = response.result.data.filter((val: any) => {
                        const _id = val["id"];
                        return !items.some(obj => obj['id'] === _id)

                    });
                    if (replace){
                        localItems = [...newItems];
                    } else {
                        localItems = [...items, ...newItems];
                    }

                    const page = response.result.pagination?.page || 1;
                    const pages = response.result.pagination?.pages || 1;

                    setRequestState({
                        params: {
                            ...(params || requestState.params),
                            page: page < pages ? page + 1 : pages
                        },
                        hasMore: page < pages
                    })
                } else {
                    setRequestState({
                        ...requestState,
                        hasMore: false
                    })
                }
                setItems(localItems);
            });
            setLoading(false);

        }
        return localItems
    };

    const loadDefault = async () => {
        if (defaultValue && defaultValue.length > 0 && optionHandler) {
            let localItems: any[] = [];

            try {
                setLoadDefaultState(true);
                await Request(optionHandler, {
                    ...requestState.params,
                    id: defaultValue,
                    per_page: defaultValue.length
                }).then((response: any) => {
                    if (response.result?.data) {
                        const newItems = response.result.data.filter((val: any) => {
                            const _id = val["id"];
                            return !items.some(obj => obj['id'] === _id)

                        });
                        localItems = [...items, ...newItems];
                    }
                });

                const dItems = defaultValue.filter((val: string) => {
                    return !localItems.some(obj => obj['id'] === val)
                }).map((val: string) => {
                    return {id: val, label: val + ` : ${i18n.t('label.record_not_found')}`};
                });

                setItems([...dItems, ...localItems]);
                setLoading(false);

            } finally {
                setLoadDefaultState(false)
            }
        }
    };
    const scrollFetching = async (overscanStopIndex: number, visibleStopIndex: number) => {
        console.log(overscanStopIndex)
        if (visibleStopIndex > items.length * 0.97 && !scrollState.fetch && requestState.hasMore) {
            setScrollState({
                ...scrollState,
                offset: visibleStopIndex,
                fetch: true
            });
            await fetchData();

            setScrollState({
                isScrolled: false,
                fetch: false,
                offset: visibleStopIndex
            })
        }
    };

    return [{
        loadDefaultState,
        items,
        loading,
        scrollState,
        loadDefault,
        fetchData,
        scrollFetching,
        setScrollState,
        queryFetch
    }];
};


const Select = (props: SelectInterface) => {
    const {style, className, onChange, cleanable, renderExtraFooter, searchable, disabled, placeholder, optionHandler, params, renderValue, container, Component = SelectPicker} = props;
    let {value} = props;

    let defaultValue: string[] = [];

    if (value && !Array.isArray(value)) {
        defaultValue = [value]
    } else if (Array.isArray(value)){
        defaultValue = value
    }
    const listRef = React.useRef<PickerHandle>(null);
    const [{
        loadDefaultState,
        items,
        loading,
        scrollState,
        loadDefault,
        fetchData,
        scrollFetching,
        setScrollState,
        queryFetch,
    }] = useData(optionHandler, params, defaultValue);


    React.useEffect(() => {
        loadDefault();
    }, []);

    const renderMenu = (menu: any) => {
        if (loading) {
            return (
                <>
                    {items.length > 0 && menu}
                    <p className={"p-4 text-center"}>
                        <SpinnerIcon spin/> {i18n.t('label.loading')}...
                    </p>
                </>
            );
        }
        return menu
    };

    return (
        <Component
            block
            loading={loadDefaultState}
            style={style}
            className={className}
            placeholder={placeholder}
            renderValue={renderValue}
            container={container}
            // virtualized
            ref={listRef}
            listProps={{
                onScroll: () => {
                    if (!scrollState.fetch && !scrollState.isScrolled && listRef.current?.list?.scrollToRow) {
                        listRef.current?.list?.scrollToRow(scrollState.offset);
                        setScrollState({
                            ...scrollState,
                            isScrolled: true
                        });
                    }
                },
                onItemsRendered: async (props: any) => {
                    const {overscanStopIndex, visibleStopIndex} = props;
                    await scrollFetching(overscanStopIndex, visibleStopIndex)
                }

            }}
            data={items}
            // placement={"autoVerticalStart"}
            searchBy={() => {
                return true
            }}
            cleanable={cleanable || true}
            searchable={searchable}
            onChange={onChange}
            disabled={disabled || false}
            renderExtraFooter={renderExtraFooter}
            defaultValue={value}
            valueKey={"id"}
            onEnter={() => {
                return fetchData()
            }}
            onExiting={()=> {
               return queryFetch('')
            }}
            onSearch={(q) => {
                return queryFetch(q)
            }}
            renderMenu={renderMenu}
        />
    );
};

export {Select}
// export {SelectInterface}
