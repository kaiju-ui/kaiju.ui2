import React from "react";
import {FlexboxGrid, Col} from "rsuite";

interface GridInterface {
    id: any,
    icon: React.ReactNode,
    label: string,
}

const FlexGrid: React.FC<{ grid: any[], onClick: (id: any) => void }> = ({grid, onClick}) => {

    return (
        <FlexboxGrid align={'middle'}>
            {grid.map((element: GridInterface) => {
                return (
                    <FlexboxGrid.Item as={Col}
                        key={element.id}
                        colspan={8}
                        md={8}
                        xs={12}
                        style={{border: '1px solid #dee2e6'}}
                        className={'item'}
                        onClick={() => {
                            onClick(element.id)
                        }}

                    >
                        {element.icon}
                        <div className={'grid-label fs-16'}>{element.label}</div>
                    </FlexboxGrid.Item>
                )
            })}

        </FlexboxGrid>
    )
};

export {FlexGrid};